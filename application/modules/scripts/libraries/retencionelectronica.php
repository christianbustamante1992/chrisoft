<?php

if (!defined('BASEPATH'))
    exit('No esta permitido el acceso');

include_once("soapclientauth.php");

//La primera línea impide el acceso directo a este script
class RetencionElectronica {
    protected $xml = null;
    protected $path_xml_generado;
    protected $facturaCompra;
    protected $cliente;
    protected $impuestos;
    protected $tipo_identificacion;
    protected $ambiente;
    protected $emision;
    protected $ci;
    protected $empresa;
    protected $retencion;
    protected $server;
    protected $contribuyente;
    protected $url_ws_recepcion;
    protected $url_ws_autorizacion;
    protected $secuencia;

    /* Parametros del constructor:
    Ambiente = 1 pruebas, 2 produccion
    Tipo de Emision = Normal 1, Emisión por Indisponibilidad del Sistema=2 */
    function __construct($params){
        $this->url_ws_recepcion=$params['url_recepcion'];
        $this->url_ws_autorizacion=$params['url_autorizacion'];
        $this->ambiente = $params['ambiente_send'];
        $this->emision = 1;
        $this->ci = & get_instance();
        $this->encript = new encript();
        $this->server = 'http://localhost:8000/MavenEnterpriseApp-web/RetencionXml?WSDL';
        $this->path_xml_generado = 'c:\\archivos\\';
        $this->fecha_emision="";
        $this->numero_secuencia=0;
    }

    /*
     * Metodo que carga la informacion de la factura que fue generada y posteriormente inicia el proceso
     * de armado del archivo xml
     */

    public function generar($id_factura,$secuencia) {
        $this->numero_secuencia=$secuencia;
        $this->facturaCompra = $this->ci->generic_model->get_by_id('billing_facturacompra', $id_factura,'','codigoFacturaCompra');

        $this->retencion = $this->ci->generic_model->get_by_id('bill_retencion', $this->facturaCompra->retencion_id,'','id');
        $this->proveedor = $this->ci->generic_model->get_by_id('billing_proveedor', $this->facturaCompra->proveedor_id,'','id');
        
        $id_identificacion_tipo = $this->proveedor->docidentificacion_id;

        
        $this->tipo_identificacion = $this->ci->generic_model->get_by_id('billing_docidentificaciontipo', $id_identificacion_tipo,'','iddocidentificaciontipo');
                
        $this->impuestos = $this->ci->generic_model->get('bill_retencion_det', array('retencion_id'=>$this->retencion->id), '', null );
        
        $this->generar_cabezera_xml();
        $this->generar_info_tributaria();
        
        $this->generar_info_retencion();
        $this->generar_impuestos();
        $this->info_adicional();
        $this->guardar_xml();

        return $this->send_file();
    }

    /*
     * Metodo que crea un calse SimpleXMLElement que sería el nodo raiz del xmnl
     */

    function generar_cabezera_xml() {
        $this->xml = new SimpleXMLElement('<comprobanteRetencion id="comprobante" version="1.0.0"></comprobanteRetencion>');
    }

    /*
     * Funcion genera la informacion para el nodo infoTributaria, esta informacion se refiera a datos relacionados
     * con datos de la empresa que emite la factura.
     */

    function generar_info_tributaria() {
        $this->empresa = $this->ci->generic_model->get_by_id('billing_empresa', 10, '', 'id');
        $this->contribuyente=$this->ci->generic_model->get_by_id('bill_contribuyente', $this->empresa->contribuyente_id, '', 'id');

        $info_tributaria = $this->xml->addChild("infoTributaria");
        $info_tributaria->addChild('ambiente', $this->ambiente);
        $info_tributaria->addChild('tipoEmision', $this->emision);
        if($this->ambiente==1){
            $info_tributaria->addChild('razonSocial', 'PRUEBAS SERVICIO DE RENTAS INTERNAS');
        }else{
            $info_tributaria->addChild('razonSocial', trim($this->empresa->razonSocial));
        }
        $info_tributaria->addChild('nombreComercial', $this->empresa->nombreComercial);
        $info_tributaria->addChild('ruc', $this->encript->decryptbase64($this->empresa->ruc,PASSWORDSALTMAIN));
        $info_tributaria->addChild('claveAcceso', "1111111111111111111111111111111111111111111111111");
        $info_tributaria->addChild('codDoc', str_pad(7,2,"0",STR_PAD_LEFT));
        $info_tributaria->addChild('estab', $this->retencion->establecimiento);
        $info_tributaria->addChild('ptoEmi', $this->retencion->pemision);
        $info_tributaria->addChild('secuencial', str_pad($this->numero_secuencia, 9, "0", STR_PAD_LEFT));
        $info_tributaria->addChild('dirMatriz', $this->empresa->direccion);
    }

    /*
     * Metodo que genera la informacion para el nodo infoFactura, este informacion esta relacionada
     * al datos generales de la factura
     */

    function generar_info_retencion() {
        $info_factura = $this->xml->addChild("infoCompRetencion");
        date_default_timezone_set("America/Guayaquil");
        $fecha_actual = date('d/m/Y', time());
        $info_factura->addChild('fechaEmision', $fecha_actual);
        $info_factura->addChild('dirEstablecimiento', $this->proveedor->direccion);
//      Campo no obligatorio
        if($this->contribuyente->clase=="ESPECIAL"){
            $info_factura->addChild('contribuyenteEspecial', $this->contribuyente->resolucion);
        }
//      Campo no obligatorio
        if($this->contribuyente->contabilidad=="SI"){
            $info_factura->addChild('obligadoContabilidad', $this->contribuyente->contabilidad);
        }

        $info_factura->addChild('tipoIdentificacionSujetoRetenido', $this->tipo_identificacion->cod);
        if($this->tipo_identificacion->cod=='07'){
            $info_factura->addChild('razonSocialSujetoRetenido', 'CONSUMIDOR FINAL');
            $info_factura->addChild('identificacionSujetoRetenido', str_pad(9,13,"9",STR_PAD_LEFT));
        }else{
            $proveedor = $this->proveedor->apellidos.' '.$this->proveedor->nombres;
            if($this->ambiente==1){
                $info_factura->addChild('razonSocialSujetoRetenido', trim($proveedor));
                
            }else{
                $info_factura->addChild('razonSocialSujetoRetenido', trim($proveedor));
            }
            $info_factura->addChild('identificacionSujetoRetenido', $this->proveedor->PersonaComercio_cedulaRuc);
        }

//      Campo no obligatorio
        $fechaemision=explode('-', $this->facturaCompra->fechaemisionfactura);
        $info_factura->addChild('periodoFiscal', $fechaemision[1].'/'.$fechaemision[0]);
    }

    /*
     * Se genera la informacion para el nodo detalles, referente a los detalles del comprobante
     */

    function generar_impuestos() {
        $impuestos = $this->xml->addChild("impuestos");

        foreach ($this->impuestos as $item) {
            $impuesto = $impuestos->addChild('impuesto');
            if($item->impuesto_renta =='RENTA'){
                $impuesto ->addChild('codigo',1);    
            }else{
                if($item->impuesto_renta =='IVA'){
                    $impuesto ->addChild('codigo',2);    
                }else{
                    $impuesto ->addChild('codigo',6);    
                }
            }
            $impuesto ->addChild('codigoRetencion',$item->cod_impuesto);    
            $impuesto ->addChild('baseImponible',$item->base_imponible);
            $impuesto ->addChild('porcentajeRetener',$item->porcent_ret);
            $impuesto ->addChild('valorRetenido',$item->val_retenido);
            $impuesto ->addChild('codDocSustento',$this->facturaCompra->sustento_cod);
            $numcodsustento=$this->facturaCompra->pa_sriautorizaciondocs_establecimiento.$this->facturaCompra->pa_sriautorizaciondocs_puntoemision.str_pad($this->facturaCompra->noFacturaCompra, 9,"0",STR_PAD_LEFT);
            $impuesto ->addChild('numDocSustento',$numcodsustento);
            $fecha=explode('-', $this->facturaCompra->fechaemisionfactura);
            $impuesto ->addChild('fechaEmisionDocSustento',$fecha[2].'/'.$fecha[1].'/'.$fecha[0]);
        }
    }

    function info_adicional(){
        /*$adicional = $this->xml->addChild("infoAdicional");
        $adicional ->addChild('campoAdicional', );
        $adicional ->addChild('campoAdicional', );
        $adicional ->addChild('campoAdicional', );*/
    }

   
    function guardar_xml() {
        $dom_sxe = dom_import_simplexml($this->xml);
        if (!$dom_sxe) {
            echo 'Error al convertir a XML';
            exit;
        }

        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom_sxe = $dom->importNode($dom_sxe, true);
        $dom_sxe = $dom->appendChild($dom_sxe);
        $dom->formatOutput = true;
        $el_xml = $dom->saveXML();
        $dom->save($this->path_xml_generado.'retencion.xml');
    }

    /*
     * Funcion que permite enviar el archivo xml generado al WebService de facturacion, y este
     * posteriormente lo enviara el web service del sri
     */

    public function send_file() {
        return $this->webService($this->path_xml_generado.'retencion.xml');
    }

    function webService($filename) {
//        Se inicia una clase SoapClientAuth, donde se fija la direccion del servicio web y crea un cliente soap
//        para el web service establecido
        libxml_disable_entity_loader(false);
        $soapClient = new SoapClientAuth($this->server, array(
            'login' => 'username',
            'password' => 'password'
        ));

//        Se verifica que exita el archivo xml y posteriormente se lo transforma a un arreglo 
//        de bytes de base 64
        if (!($fp = fopen($filename, "r"))) {
            echo "Error opening file";
            exit;
        }
        $data = "";
        while (!feof($fp)) {
            $data .= fread($fp, 1024);
        }
        fclose($fp);
        $byteFile = base64_encode($data);
//        Se establecen los variables y sus valores para ser enviados al metodo recepcion 
//        expuesto en el web service, el mismo que nos devolvera un mensaje con informacion
//        referente al proceso de facturacion electronica
        $email='';
        if($this->ambiente==1){
            $email='jaimejhonle@gmail.com';
        }else{
            $email=$this->proveedor->email;
        }
        $message = array(
                        'buffer' => $byteFile, 
                        'user' => 'admin',
                        'email'=>$email,
                        'direccion'=>$this->proveedor->direccion,
                        'telefonos'=>$this->proveedor->telefonos,
                        'url_recepcion'=>$this->url_ws_recepcion,
                        'url_autorizacion'=>$this->url_ws_autorizacion,
                        'razonsocial'=>$this->proveedor->razonsocial,
                        'nro_doc'=>$this->facturaCompra->pa_sriautorizaciondocs_establecimiento.$this->facturaCompra->pa_sriautorizaciondocs_puntoemision.$this->facturaCompra->noFacturaCompra,
                        'nombredoc'=>'FACTURA',
                        'claveacceso'=>$this->retencion->cod_fact_electronica
                        );
        $respuesta = $soapClient->recepcion_retencion_xml($message)->{"return"};
        return $respuesta;
    }
}