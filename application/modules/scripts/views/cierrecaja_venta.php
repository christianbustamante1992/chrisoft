<?php
echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/asiento_contable_det_x_ventas'), 'method' => 'post'));
    
    echo tagcontent('button', 'VENTAS', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-1 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/asiento_contable_det_x_compras'), 'method' => 'post'));
    
    echo tagcontent('button', 'COMPRAS', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-1 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/tipo_proveedor'), 'method' => 'post'));
    
    echo tagcontent('button', 'TIPO DE PROVEEDOR', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-2 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/doc_identificacion_vacio'), 'method' => 'post'));
    
    echo tagcontent('button', 'DOC IDENTIFICACION VACIO', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-3 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/igualar_decimales_compras'), 'method' => 'post'));
    
    echo tagcontent('button', 'DECIMALES COMPRA', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-2 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo Open('form', array('action' => base_url('scripts/cierrecaja_venta/igualar_decimales_ventas'), 'method' => 'post'));
    
    echo tagcontent('button', 'DECIMALES VENTA', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-2 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));

echo Close('form');

echo tagcontent('div', '', array('id' => 'products_out', 'class' => 'col-md-12'));
?>