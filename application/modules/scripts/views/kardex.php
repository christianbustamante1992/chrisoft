<?php
echo Open('div',array('id' => 'result', 'class' => 'col-md-12'));?>
        <div class="panel-body">
            <table class="table table-striped table-condensed" style='font-size:12px'><?php
                echo tablethead(array('Kardex','Kardex Total','Transaccion','ESTADO'));
                        if ($kardex != NULL){
                            foreach ($kardex as $value) {
                                echo Open('tr');
                                    echo tagcontent('td',$value->kardex);
                                    echo tagcontent('td',$value->kardex_total);
                                    //echo tagcontent('td',$value->nombreunico);
                                    echo tagcontent('td',$value->nombre);
                                    if($value->estado==1){
                                        echo tagcontent('td','NORMAL');
                                    }
                                    if($value->estado==-1){
                                        echo tagcontent('td','ANULADO');
                                    }
                                    if($value->estado==-3){
                                        echo tagcontent('td','PUNTO DE ANULACION');
                                    }
                                    if($value->estado==-2){
                                        echo tagcontent('td','REEMPLAZADO');
                                    }
                                    if($value->estado==2){
                                        echo tagcontent('td','ABRIR');
                                    }
                                echo Close('tr');
                            }
                        }
            echo Close('table');
echo Close('div');
echo Open('form', array('action' => base_url('scripts/kardex/generar'), 'method' => 'post'));
    echo input(array('type' => 'hidden', 'name' => 'codigo','value'=>$codigo));
    echo tagcontent('button', 'Generar', array('name' => 'btnGenerar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
echo Close('form');