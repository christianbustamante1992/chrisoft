<div class="navbar-defaults sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
                <!-- /input-group -->
            </li>
            
             <li><a class="active" href="<?= base_url('scripts/cierrecaja_venta')?>">Cierre Caja Venta</a></li>
             <li><a href="<?= base_url('scripts/facturacionelectronica')?>">Facturacion Electronica</a></li>
             <li><a href="<?= base_url('scripts/retencion')?>">Retencion Electronica</a></li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->