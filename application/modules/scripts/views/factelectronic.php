<?php
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/facturacionelectronica/secuencia'), 'method' => 'post'));
	    echo tagcontent('button', 'EJECUTAR', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md-1 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo tagcontent('br');
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/facturacionelectronica/fecha_archivada'), 'method' => 'post'));
	    echo 'Se actualiza nueva campo Fecha Firmada con el campo Fecha Archivada';
	    echo tagcontent('button', 'Fecha Firmada', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/facturacionelectronica/facturas_campo_oculto'), 'method' => 'post'));
	    echo tagcontent('button', 'Oculto', array('name' => 'btnIngresar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/facturacionelectronica/generar_archivos'), 'method' => 'post'));
		$input1 = input(array('type' => 'text', 'name' => 'codigo', 'placeholder' => '', 'class' => 'form-control','value'=>''));
	    echo get_combo_group('Codigo Factura', $input1, $class = 'col-md-3 form-group');
	    echo tagcontent('button', 'Generar', array('name' => 'btnGenerar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/facturacionelectronica/generar_archivos_x_fecha'), 'method' => 'post'));
		$text_inputs = array(
        '0' => array('type' => 'text', 'name' => 'fechaIn', 'placeholder' => 'Inicio', 'class' => 'form-control datepicker','value'=>'','style'=>'width:50%'),
        '1' => array('type' => 'text', 'name' => 'fechaFin', 'placeholder' => 'Fin', 'class' => 'form-control datepicker','value'=>'','style'=>'width:50%')
    	);
    	echo get_field_group('Fechas:', $text_inputs, $class = 'col-md-4 form-group');
    	echo tagcontent('button', 'Generar', array('name' => 'btnGenerar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo Open('div', array('class'=>'col-md-12'));
	echo Open('form', array('action' => base_url('scripts/api/load_file'), 'method' => 'post'));
		echo tagcontent('button', 'GOOGLE', array('name' => 'btnGenerar', 'class' => 'btn btn-primary col-md21 pull-left','id' => 'ajaxformbtn', 'type' => 'submit', 'data-target' => 'products_out'));
	echo Close('form');
echo Close('div');
echo tagcontent('div', '', array('id' => 'products_out', 'class' => 'col-md-12'));