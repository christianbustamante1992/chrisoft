<div class="panel panel-success">
    <div class="panel-heading">RESUMEN DE ENVIOS :</div>
      <div class="panel-body">
        <table class="table table-striped table-success">
            <tr>
                <th>ENVIADOS</th>
                <th>AUTORIZADOS</th>
                <th>RECHAZADOS</th>
            </tr>
                <tr>
                    <td><?php echo $total; ?></td>
                    <td><?php echo $ok; ?></td>
                    <td><?php echo $fail; ?></td>
                </tr>
        </table>
</div>