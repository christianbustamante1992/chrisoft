<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CierreCaja_Venta extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $res['view']=$this->load->view('cierrecaja_venta','',TRUE);
        $res['slidebar'] = $this->load->view('slidebar','',TRUE);
        $this->load->view('common/templates/dashboard',$res);        
    }

    public function asiento_contable_det_x_ventas(){
        set_time_limit(0);

        /*$where_data = array('MONTH(fv.fecha_firmada)'=>'01','fv.estado'=>2,'fv.autorizado_sri'=>1);
        $join_cluase = array(
                                '0'=>array('table'=>'bill_asiento_contable ac','condition'=>'ac.doc_id=fv.codigofactventa and ac.estado>0 and ac.tipotransaccion_cod = 01'),
                                '1'=>array('table'=>'bill_asiento_contable_det acd','condition'=>'acd.asiento_contable_id=ac.id')
                            );
        $fields = array('acd.*');
        $detalle_asiento = $this->generic_model->get_join('billing_facturaventa fv',$where_data , $join_cluase,$fields);

    	foreach ($detalle_asiento as $value) {
    		$detalle = $this->generic_model->get('bill_ventatipopago', array('venta_id'=>$value->doc_id),'');
    		foreach ($detalle as $key) {
    			if($value->debito==$key->valor){
    				$this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>$key->tipopago_id), $value->id, 'id' );
    				print_r($value->doc_id.'>>>>>>>>>>>>>>>>>>'.$value->debito.'=='.$key->valor);echo '<br>';
    			}
    		}
    	}*/
        /*$where_data = array('MONTH(fv.fecha_firmada)'=>'01','fv.estado'=>2,'fv.autorizado_sri'=>1);
        $join_cluase = array(
                                '0'=>array('table'=>'bill_asiento_contable ac','condition'=>'ac.doc_id=fv.codigofactventa and ac.estado>0 and ac.tipotransaccion_cod = 01 and ac.id=2739'),
                                '1'=>array('table'=>'bill_asiento_contable_det acd','condition'=>'acd.asiento_contable_id=ac.id')
                            );
        $fields = array('acd.*');
        $detalle_asientos = $this->generic_model->get_join('billing_facturaventas fv',$where_data , $join_cluase,$fields);
        foreach ($detalle_asientos as $key => $detalle_asiento) {
            if($detalle_asiento->cuenta_cont_id == '1010101'){
                $this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>'01'), $detalle_asiento->id, 'id' );
            }
        }*/

        /*$where_data = array('ac.estado'=>1, 'ac.tipotransaccion_cod' => "01",'MONTH(ac.fecha)'=>'01');
        $join_cluase = array(
                                '0'=>array('table'=>'bill_asiento_contable_det acd','condition'=>'acd.asiento_contable_id=ac.id and acd.cuenta_cont_id = "1010101"')
                            );
        $fields = array('acd.id');
        $detalle_asientos = $this->generic_model->get_join('bill_asiento_contable ac',$where_data , $join_cluase,$fields);
        foreach ($detalle_asientos as $key => $detalle_asiento) {
            $this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>'01'), $detalle_asiento->id, 'id' );
        }

        $where_data = array('ac.estado'=>1, 'ac.tipotransaccion_cod' => "18",'MONTH(ac.fecha)'=>'01');
        $join_cluase = array(
                                '0'=>array('table'=>'bill_asiento_contable_det acd','condition'=>'acd.asiento_contable_id=ac.id and acd.cuenta_cont_id = "1010101"')
                            );
        $fields = array('acd.id');
        $detalle_asientos = $this->generic_model->get_join('bill_asiento_contable ac',$where_data , $join_cluase,$fields);
        foreach ($detalle_asientos as $key => $detalle_asiento) {
            $this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>'01'), $detalle_asiento->id, 'id' );
        }*/

        $where_data = array('ac.estado'=>1, 'ac.tipotransaccion_cod' => "05",'MONTH(ac.fecha)'=>'01');
        $join_cluase = array(
                                '0'=>array('table'=>'bill_asiento_contable_det acd','condition'=>'acd.asiento_contable_id=ac.id and acd.cuenta_cont_id = "1010101"')
                            );
        $fields = array('acd.id');
        $detalle_asientos = $this->generic_model->get_join('bill_asiento_contable ac',$where_data , $join_cluase,$fields);
        foreach ($detalle_asientos as $key => $detalle_asiento) {
            $this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>'01'), $detalle_asiento->id, 'id' );
        }
    	echo 'TERMINO EL SCRIPT';
    }

    public function asiento_contable_det_x_compras(){
        set_time_limit(0);
        $where_data=array('tipotransaccion_cod'=>'02');
        $detalle_asiento=$this->generic_model->get('bill_asiento_contable_det', $where_data , $fields = '');
        foreach ($detalle_asiento as $value) {
            $detalle = $this->generic_model->get('bill_compratipopago', array('compra_id'=>$value->doc_id),'');
            foreach ($detalle as $key) {
                if($value->credito==$key->valor){
                    $this->generic_model->update_by_id('bill_asiento_contable_det', array('tipo_pago'=>$key->tipopago_id), $value->id, 'id' );
                    print_r($value->doc_id.'>>>>>>>>>>>>>>>>>>'.$value->credito.'=='.$key->valor);echo '<br>';
                }
            }
        }
        echo 'TERMINO EL SCRIPT';
    }

    public function transferencias(){
        set_time_limit(0);
        $transferencias=$this->generic_model->get('bill_transferencia', null, $fields = '');
        foreach ($transferencias as $value) {
            $detalle=$this->generic_model->get('bill_transferenciaproducto', array('transferencia_id'=>$value->id), $fields = '')[0];
            $this->generic_model->update_by_id('bill_transferencia', array('bodega_ori'=>$detalle->bodega_ori,'bodega_fin'=>$detalle->bodega_fin), $value->id, 'id' );
        }
        echo 'TERMINO EL SCRIPT';
    }

    public function tipo_proveedor(){
        set_time_limit(0);
        $cont=0;
        $rta='';
        $this->load->library('docident');
        $this->docident=new docident();
        $proveedor=$this->generic_model->get('billing_proveedor p',array('LENGTH(p.PersonaComercio_cedulaRuc)'=>13), $fields = '');
        echo '>>>>>>>'.count($proveedor).'<br>';
        foreach ($proveedor as $value) {
            if($this->docident->validarRucPersonaNatural($value->PersonaComercio_cedulaRuc)==TRUE){
                $rta='01';
            }else{
                if($this->docident->validarRucSociedadPrivada($value->PersonaComercio_cedulaRuc)==TRUE){
                    $rta='02';
                }else{
                    if($this->docident->validarRucSociedadPublica($value->PersonaComercio_cedulaRuc)==TRUE){
                        $rta='01';
                    }else{
                        $rta='00';
                    }
                }
            }
            $cont += $this->generic_model->update_by_id('billing_proveedor p',array('tipoproveedor_id'=>$rta), $value->PersonaComercio_cedulaRuc, 'p.PersonaComercio_cedulaRuc' );
        }

        

        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.$cont;
    }

    public function doc_identificacion_vacio(){
        set_time_limit(0);
        $cont=0;
        $rta='';
        $this->load->library('docident');
        $this->docident=new docident();
        $proveedor=$this->generic_model->get('billing_proveedor p',array('LENGTH(p.PersonaComercio_cedulaRuc)'=>10), $fields = '');
        echo 'CEDULA >>>>>>>'.count($proveedor).'<br>';
        foreach ($proveedor as $value) {
            $cont += $this->generic_model->update_by_id('billing_proveedor p',array('docidentificacion_id'=>2), $value->PersonaComercio_cedulaRuc, 'p.PersonaComercio_cedulaRuc' );
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.$cont.'<br>';
        $proveedor=$this->generic_model->get('billing_proveedor p',array('LENGTH(p.PersonaComercio_cedulaRuc)'=>13), $fields = '');
        echo 'RUC >>>>>>>'.count($proveedor).'<br>';
        foreach ($proveedor as $value) {
            $cont += $this->generic_model->update_by_id('billing_proveedor p',array('docidentificacion_id'=>1), $value->PersonaComercio_cedulaRuc, 'p.PersonaComercio_cedulaRuc' );
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.$cont.'<br>';
        $proveedor=$this->generic_model->get('billing_proveedor p',array('LENGTH(p.PersonaComercio_cedulaRuc) <'=>10), $fields = '');
        echo 'PASAPORTE >>>>>>>'.count($proveedor).'<br>';
        foreach ($proveedor as $value) {
            $cont += $this->generic_model->update_by_id('billing_proveedor p',array('docidentificacion_id'=>3), $value->PersonaComercio_cedulaRuc, 'p.PersonaComercio_cedulaRuc' );
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.$cont.'<br>';
    }

    public function igualar_decimales_compras(){
        set_time_limit(0);
        $compras=$this->generic_model->get('billing_facturacompra fc',array('MONTH(fc.fechaemisionfactura)'=>01), $fields = '');
        foreach ($compras as $value) {
            $iva=number_decimal($value->baseiva*0.12);
            if(number_decimal($value->iva)!=$iva){
                $this->generic_model->update_by_id('billing_facturacompra fc',array('iva'=>$iva), $value->codigoFacturaCompra, 'fc.codigoFacturaCompra' );
            }
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';
    }

    public function igualar_decimales_ventas(){
        set_time_limit(0);
        $ventas=$this->generic_model->get('billing_facturaventa fv',array('MONTH(fv.fecha_firmada)'=>01), $fields = '');
        foreach ($ventas as $value) {
            $iva=$value->tarifadoceneto*0.12;
            //if($value->ivaval!=$iva){
                $this->generic_model->update_by_id('billing_facturaventa fv',array('ivaval'=>$iva), $value->codigofactventa, 'fv.codigofactventa' );
            //}
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';
    }
}