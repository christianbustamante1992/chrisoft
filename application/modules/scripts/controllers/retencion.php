<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Retencion extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $res['view']=$this->load->view('retencion','',TRUE);
        $res['slidebar'] = $this->load->view('slidebar','',TRUE);
        $this->load->view('common/templates/dashboard',$res);        
    }
    
    public function faltantes2(){
        set_time_limit(0);
        $where_data=array(
            'estado'=>'2','autorizado_sri ='=>1,'puntoventaempleado_establecimiento'=>'001','puntoventaempleado_puntoemision'=>'006',
            'MONTH(fecha_firmada)'=>02
            );
        $limite=$this->generic_model->count_all_results('billing_facturaventa', $where_data );
        $secuencia_max = $this->generic_model->get('billing_facturaventa', $where_data, $fields = 'MAX(secuenciafactventa) max')[0]->max;
        $secuencia_min = $this->generic_model->get('billing_facturaventa', $where_data, $fields = 'MIN(secuenciafactventa) min')[0]->min;
        $cont=0;
        for ($i=$secuencia_min; $i <= $secuencia_max; $i++) { 
            $where_data=array('secuenciafactventa'=>$i);
            $rta=$this->generic_model->count_all_results('billing_facturaventa', $where_data );
            if($rta==0){
                echo $i.'<BR>';
                $cont++;
            }
        }
        echo 'CONTADOR'.$cont.'<BR>';
        echo 'TOTAL FACTURAS >>>>>>>'.$limite;
    }

    public function fecha_archivada(){
        set_time_limit(0);
        $where_data=array('estado'=>'2','autorizado_sri'=>1,'puntoventaempleado_establecimiento'=>'001','puntoventaempleado_puntoemision'=>'006');
        $rta=$this->generic_model->get('billing_facturaventa', $where_data = null, $fields = '', $order_by = null, $rows_num = 0, $or_like_data = null, $and_like_data = null, $group_by = null, $or_where = null );
        $count=$this->generic_model->count_all_results('billing_facturaventa', $where_data , $or_where_data = null );
        echo 'TOTAL DE REGISTROS >>>>>>>>>>> '.$count;echo '<br>';
        $contador=0;
        foreach ($rta as $value) {
            $contador+=$this->generic_model->update_by_id('billing_facturaventa', array('fecha_firmada'=>$value->fechaarchivada), $value->codigofactventa, $id_column_name = 'codigofactventa' );
            print_r($value->fechaarchivada);echo '<br>';
            print_r($value->codigofactventa);echo '<br>';
            break;
        }
        echo 'REGISTROS ACTUALIZADOS >>>>>>>>>>>>>>>>>> '.$contador;
    }

    public function facturas_campo_oculto(){
        set_time_limit(0);
        $url_ws_recepcion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
        $url_ws_autorizacion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
        $config=array('url_recepcion'=>$url_ws_recepcion,'url_autorizacion'=>$url_ws_autorizacion,'ambiente_send'=>2);
        $this->load->library('factelectronica',$config);
        $where_data=array('fvd.detalle !='=>"");
        $join_cluase=array(
                '0'=>array('table'=>'billing_facturaventa fv','condition'=>'fv.codigofactventa=fvd.facturaventa_codigofactventa and fv.autorizado_sri=1'));
        $fields=array('distinct(fv.codigofactventa) codigo','fvd.detalle','fv.fecha_firmada fecha','fv.secuenciafactventa secuencia');
        $facturas=$this->generic_model->get_join('billing_facturaventadetalles fvd',$where_data,$join_cluase, $fields);
        //print_r('REGISTROS >>>>>>>>>>> '.count($facturas));echo '<br>';
        foreach ($facturas as $value) {
            if($value->detalle!='0'){
                print_r($value->codigo);echo '<br>';
                $this->factelectronica->generar($value->codigo,date_format(date_create($value->fecha),'d/m/Y') ,$value->secuencia);
            }
        }
        
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';
    }

    public function generar_archivos(){
        /*set_time_limit(0);
        $url_ws_recepcion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
        $url_ws_autorizacion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
        $config=array('url_recepcion'=>$url_ws_recepcion,'url_autorizacion'=>$url_ws_autorizacion,'ambiente_send'=>2);
        $this->load->library('retencionelectronica',$config);
        $where=array('fc.noFacturacompra'=>$this->input->post('codigo'));
        $fields=array('fc.noFacturacompra secuencia','fc.codigoFacturaCompra id');
        $retenciones=$this->generic_model->get('billing_facturacompra fc', $where, $fields);
        foreach ($retenciones as $value) {
            $this->retencionelectronica->generar($value->id,$value->secuencia);
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';*/

        set_time_limit(0);
        $url_ws_recepcion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
        $url_ws_autorizacion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
        $config=array('url_recepcion'=>$url_ws_recepcion,'url_autorizacion'=>$url_ws_autorizacion,'ambiente_send'=>2);
        $this->load->library('retencionelectronica',$config);
        $where=array('r.nro'=>$this->input->post('codigo'));
        $join_cluase=array(
                '0'=>array('table'=>'billing_facturacompra fc','condition'=>'fc.codigoFacturaCompra=r.doc_id'));
        $fields=array('fc.noFacturacompra secuencia','fc.codigoFacturaCompra id');
        $retenciones=$this->generic_model->get_join('bill_retencion r','',$join_cluase, $fields);
        $this->retencionelectronica->generar(1424,424);
        /*print_r('>>>>'.$retenciones[0]->id);echo '<br>';
        print_r('>>>>'.$retenciones[0]->secuencia);echo '<br>';*/
        /*foreach ($retenciones as $value) {
            $this->retencionelectronica->generar($value->id,$value->secuencia);
        }*/
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';
    }

    public function generar_archivos_x_fecha(){
        set_time_limit(0);
        $url_ws_recepcion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
        $url_ws_autorizacion="https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
        $config=array('url_recepcion'=>$url_ws_recepcion,'url_autorizacion'=>$url_ws_autorizacion,'ambiente_send'=>2);
        $this->load->library('retencionelectronica',$config);
        $where=array('r.fecha >='=>$this->input->post('fechaIn'));
        $where=array('r.fecha <='=>$this->input->post('fechaFin'));
        $join_cluase=array(
                '0'=>array('table'=>'billing_facturacompra fc','condition'=>'fc.codigoFacturaCompra=r.doc_id'));
        $fields=array('fc.noFacturacompra secuencia','fc.codigoFacturaCompra id');
        $retenciones=$this->generic_model->get_join('bill_retencion r','',$join_cluase, $fields);
        foreach ($retenciones as $value) {
            $this->retencionelectronica->generar($value->id,$value->secuencia);
        }
        echo 'TERMINO EL SCRIPT>>>>>>>>>>>>>>>>>>>>>'.'<br>';
    }
}