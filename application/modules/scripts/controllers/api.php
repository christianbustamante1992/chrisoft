<?php

class Api extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $res['view']=$this->load->view('factelectronic','',TRUE);
        $res['slidebar'] = $this->load->view('slidebar','',TRUE);
        $this->load->view('common/templates/dashboard',$res);        
    }

public function load_file() {
        //require_once realpath(dirname(__FILE__) . '/google-api-php-cliente/autoload.php');
		require_once ('C:\xampp\htdocs\billingsof_core\application\modules\common\libraries\google-api-php-cliente\autoload.php');
//      Datos obtenidos en la Consola de Desarrolladores de Google
        //$cliente_id = '792070845245-53cs74geuk3r9l467b57mbcu216c6i9c.apps.googleusercontent.com';
        $cliente_id = '723719455424-4ijale36hkhi0bh32kg5f50l1t2rkms5.apps.googleusercontent.com';
        //$email_servicio = '792070845245-53cs74geuk3r9l467b57mbcu216c6i9c@developer.gserviceaccount.com';
        $email_servicio = '723719455424-4ijale36hkhi0bh32kg5f50l1t2rkms5@developer.gserviceaccount.com';
        //$direccion_key = file_get_contents('C:/xampp/htdocs/billingsof_core/resources/key_google/Shopping Cart-466439de0a0f.p12');
        $direccion_key = file_get_contents('C:\Users\Usuario\Documents\NetBeansProjects\Drive\prueba.p12');

//      Creación de un objeto tipo Client
        $client = new Google_Client();
        $client->setClientId($cliente_id);
        if (isset($_SESSION['service_token'])) {
            $client->setAccessToken($_SESSION['service_token']);
        }
        $key = file_get_contents('C:\Users\Usuario\Documents\NetBeansProjects\Drive\prueba.p12');
        $credencial = new Google_Auth_AssertionCredentials(
                $email_servicio, array('https://www.googleapis.com/auth/drive'), $key
        );
        $client->setAssertionCredentials($credencial);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($credencial);
        }
        $_SESSION['service_token'] = $client->getAccessToken();
//      Selección del tipo de servicio en este caso Drive
        $service_drive = new Google_Service_Drive($client);
        //Creación del archivo para guardarlo en Drive
        $file = new Google_Service_Drive_DriveFile();
        $file->setTitle('prueba');
        $file->setMimeType('application/pdf');
        //Id de la carpeta en Drive
        $carpetaId = '0B3PANEQpXyWsfmpoMTlqdjZNVkdtSzFieE81MVNVMzM0emRrQTlyYXRyZEJLaHlZUHh4cHc';
        //Establecer la carpeta en donde se guardara en Drive
        if ($carpetaId != null) {
            $carpeta = new Google_Service_Drive_ParentReference();
            $carpeta->setId($carpetaId);
            $file->setParents(array($carpeta));
        }
        try {
            $data = file_get_contents('C:\Users\Usuario\Documents\NetBeansProjects\Drive\prueba.pdf');
            //Inserción del archivo en Drive
            $archivo_creado = $service_drive->files->insert($file, array(
                'data' => $data,
                'uploadType' => 'media'
            ));
//            print $archivo_creado->getId();
        } catch (Exception $e) {
            print "Se ha generado el siguiente error: " . $e->getMessage();
        }
    }
}