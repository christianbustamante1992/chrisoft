<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kardex extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $res['view']=$this->load->view('ndc','',TRUE);
        $res['slidebar'] = $this->load->view('slidebar','',TRUE);
        $this->load->view('common/templates/dashboard',$res);        
    }

    public function ver(){
        $codigo = $this->input->post('codigo');
        $where_data=array('k.producto_id'=>$codigo);
        $join_cluase=array(
            '0'=>array('table'=>'billing_producto p','condition'=>'p.codigo=k.producto_id'),
            '1'=>array('table'=>'billing_tipotransaccion tt','condition'=>'tt.cod=k.transaccion_cod')
        );
        $fields=array('k.kardex','k.kardex_total','p.nombreunico','tt.nombre','k.estado');
        $send['kardex'] = $this->generic_model->get_join('bill_kardex k',$where_data , $join_cluase,$fields);
        $send['codigo'] = $codigo;
        $this->load->view('kardex',$send);
    }

    public function generar(){
        $codigo = $this->input->post('codigo');
        $where_data=array('k.producto_id'=>$codigo);
        $join_cluase=array(
            '0'=>array('table'=>'billing_producto p','condition'=>'p.codigo=k.producto_id'),
            '1'=>array('table'=>'billing_tipotransaccion tt','condition'=>'tt.cod=k.transaccion_cod')
        );
        $fields=array('k.id','k.kardex','k.kardex_total','p.nombreunico','tt.nombre','k.estado');
        $kardex = $this->generic_model->get_join('bill_kardex k',$where_data , $join_cluase,$fields);
        //$kardex_total=$this->generic_model->get('bill_kardex',array('producto_id'=>$codigo),array('MIN(id)','kardex_total'))[0]->kardex_total;
        for ($i=1; $i < count($kardex); $i++) { 
            if($kardex[$i]->estado==-1){
                //echo 'ANULADO';echo '<br>';
                $kardex_total=$kardex[$i-1]->kardex_total;
                //print_r($kardex_total.'='.$kardex[$i-1]->kardex_total);echo '<br>';
            }else{
                if($kardex[$i]->estado==-2){
                //echo 'REEMPLAZADO';echo '<br>';
                /*$new_kardex=$kardex_total+$kardex[$i]->kardex;
                $this->generic_model->update('bill_kardex', array('kardex_total'=>$kardex[$i]->kardex), array('id'=>$kardex[$i]->id));
                $kardex_total = $this->generic_model->get_val('bill_kardex', $kardex[$i]->id, 'kardex_total', 'id');*/
                }else{
                    if($kardex[$i]->estado==-3){
                    //echo 'PUNTO ANULACION';echo '<br>';
                    $this->generic_model->update('bill_kardex', array('kardex_total'=>$kardex_total), array('id'=>$kardex[$i]->id));
                    $new_kardex_total=$kardex[$i]->kardex_total+$kardex[$i+1]->kardex;
                    $this->generic_model->update('bill_kardex', array('kardex_total'=>$new_kardex_total), array('id'=>$kardex[$i+1]->id));
                    //print_r($new_kardex_total.'='.$kardex[$i]->kardex_total.'+'.$kardex[$i+1]->kardex);echo '<br>';
                    //echo '--------------------------------------------------------------------------------------------------------';
                    }else{
                        if($kardex[$i]->estado==1){
                        //echo 'NORMAL';echo '<br>';
                        $new_kardex_total=$kardex[$i]->kardex_total+$kardex[$i+1]->kardex;
                        $this->generic_model->update('bill_kardex', array('kardex_total'=>$new_kardex_total), array('id'=>$kardex[$i+1]->id));
                        //print_r($new_kardex_total.'='.$kardex[$i]->kardex_total.'+'.$kardex[$i+1]->kardex);echo '<br>';
                        }else{
                            if($kardex[$i]->estado==2){
                            //echo 'ABRIR';echo '<br>';
                            $new_kardex_total=$kardex[$i]->kardex_total+$kardex[$i+1]->kardex;
                            $this->generic_model->update('bill_kardex', array('kardex_total'=>$new_kardex_total), array('id'=>$kardex[$i+1]->id));
                            /*print_r('bill_kardex'.'kardex_total=>'.$new_kardex_total.'array(id=>'.$kardex[$i+1]->id.')');echo '<br>';
                            print_r($new_kardex_total.'='.$kardex[$i]->kardex_total.'+'.$kardex[$i+1]->kardex);echo '<br>';
                            print($new_kardex_total);echo '<br>';
                            echo '-------------------------------------------------------------------------------------';echo '<br>';*/
                            }
                        }
                    }
                }
            }
        }
        //echo 'listo';
    }
}