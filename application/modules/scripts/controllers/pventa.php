<?php

class Pventa extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $res['view'] = $this->load->view('pventa','',TRUE);
        $res['slidebar'] = $this->load->view('slidebar','',TRUE);
        $this->load->view('common/templates/dashboard',$res);        
    }

    public function init() {
        set_time_limit(0);
        $where_data = array('YEAR(fechaCreacion)'=>2016, 'MONTH(fechaCreacion)'=>02 , 'DAY(fechaCreacion)'=>02);
        $join_cluase = array(
                                '0'=>array('table'=>'billing_facturaventadetalle fvd','condition'=>'fvd.facturaventa_codigofactventa = fv.codigofactventa')
                            );
        $fields = array('');
        $detalle = $this->generic_model->get_join('billing_facturaventa fv', $where_data , $join_cluase, $fields);
        /*$detalle = $this->generic_model->get('billing_facturaventadetalle',array('YEAR(fechaCreacion)'=>2016, 'MONTH(fechaCreacion)'=>02 , 'DAY(fechaCreacion) >'=>02));*/
        $count = 0;
        foreach ($detalle as $key => $value) {
            $where_data = array('p.codigo'=>$value->Producto_codigo);
            $join_cluase = array(
                                    '0'=>array('table'=>'bill_productoimpuestotarifa pit','condition'=>'pit.producto_id = p.codigo'),
                                    '1'=>array('table'=>'bill_impuestotarifa it','condition'=>'it.id = pit.impuestotarifa_id'),
                                    '2'=>array('table'=>'bill_impuesto i','condition'=>'i.id = it.impuesto_id')
                                );
            $fields = array('it.tarporcent porcentaje');
            $producto = $this->generic_model->get_join('billing_producto p', $where_data , $join_cluase, $fields)[0];
            $data['ivaporcent'] = $producto->porcentaje;
            /*if($value->totivaval > $value->totalpriceiva){*/
                $data['ivavalitemprecioneto'] = ($producto->porcentaje / 100) * $value->itemprecioneto;
                $data['itemprecioiva'] = (($producto->porcentaje / 100) * $value->itemprecioneto) + $value->itemprecioneto;
                $data['ivavalprecioxcantidadneto'] = (($producto->porcentaje / 100) * $value->itemprecioneto)  * $value->itemcantidad;
                $data['itemxcantidadprecioiva'] = ((($producto->porcentaje / 100) * $value->itemprecioneto) + $value->itemprecioneto) * $value->itemcantidad;

                $data['totivaval'] = ($value->itembaseiva * ($producto->porcentaje / 100)) * $value->itemcantidad;
                $data['priceiva'] = (($value->itembaseiva * ($producto->porcentaje / 100)) + $value->itembaseiva);
                $data['totalpriceiva'] = $data['priceiva']  * $value->itemcantidad;

                $this->generic_model->update_by_id('billing_facturaventadetalle', $data, $value->id, $id_column_name = 'id');
                $count++;
            /*}*/
        }
        echo 'OK >>>>>>>>>>>>>>'.$count;
    }
}